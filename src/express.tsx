import { App } from '@/components/App';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import { config } from 'dotenv';
import * as express from 'express';
import { readFileSync } from 'fs';
import * as mustache from 'mustache';
import nodeFetch from 'node-fetch';
import { exit } from 'process';
import * as React from 'react';
import { ApolloProvider, getDataFromTree } from 'react-apollo';
import { renderToString } from 'react-dom/server';
import { StaticRouter } from 'react-router';
type Request = express.Request;

config();
if (!process.env.ENDPOINT) exit();
const PORT = 3100;
const app = express();
const graphEndpoint = process.env.ENDPOINT || '';

const getClient = (req: Request) =>
  new ApolloClient({
    ssrMode: true,
    link: createHttpLink({
      fetch: nodeFetch as any,
      uri: graphEndpoint,
      credentials: 'same-origin',
      headers: {
        cookie: req.header('Cookie'),
      },
    }),
    cache: new InMemoryCache(),
  });
const template = readFileSync('public/index.mustache').toString('utf-8');
app.use(express.static('style'));
app.use(express.static('dist'));
app.get('/*', (req, res) => {
  const client = getClient(req);
  const state = { api: graphEndpoint };
  const RenderedApp = (
    <ApolloProvider client={client}>
      <StaticRouter location={req.url} context={{}}>
        <App />
      </StaticRouter>
    </ApolloProvider>
  );
  getDataFromTree(RenderedApp).then(() => {
    const reactStr = renderToString(RenderedApp);
    const html = mustache.render(template, {
      body: reactStr,
      state: JSON.stringify(state),
      apolloState: JSON.stringify(client.extract()),
    });
    res.send(html);
  });
});
app.listen(PORT, () => {
  console.log(`Express Server Listening on http://localhost:${PORT}`);
});
