import graphqlTag from 'graphql-tag';
import * as React from 'react';
import { ChildDataProps, graphql } from 'react-apollo';
import { Link } from 'react-router-dom';

const GET_ABOUT = graphqlTag`
  {
    abouts {
      name
    }
  }
`;

interface AboutState {
  name: string;
}

interface AboutResponse {
  abouts: AboutState[];
}
type AboutChildProps = ChildDataProps<{}, AboutResponse>;

const AboutRenderer = ({ data: { loading, abouts } }: AboutChildProps) => {
  if (loading || !abouts) return null;
  return (
    <React.Fragment>
      <div className="c-header__body">
        <h1>{abouts[0].name}!!!</h1>
        <Link to="/world">world</Link>
      </div>
    </React.Fragment>
  );
};
const Hello = graphql<{}, AboutResponse, {}, AboutChildProps>(GET_ABOUT)(
  AboutRenderer,
);

export { Hello };
