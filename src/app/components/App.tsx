import { Hello } from '@/components/Hello';
import { World } from '@/components/World';
import * as React from 'react';
import { Route, Switch } from 'react-router-dom';

export interface AppProps {
  api: string;
}

export const App = () => (
  <Switch>
    <Route exact path="/" component={() => <Hello />} />
    <Route path="/world" component={World} />
  </Switch>
);
