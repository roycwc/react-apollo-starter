import { App, AppProps } from '@/components/App';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import * as React from 'react';
import { ApolloProvider } from 'react-apollo';
import * as ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';

let rendering = ReactDOM.render;
let state: AppProps = { api: 'http://localhost:1337/graphql' };

if ((window as any).STATE) {
  rendering = ReactDOM.hydrate;
  state = (window as any).STATE;
}

const cache = new InMemoryCache();

if ((window as any).APOLLO_STATE) {
  rendering = ReactDOM.hydrate;
  cache.restore((window as any).APOLLO_STATE);
}
const apolloClient = new ApolloClient({
  cache,
  link: createHttpLink({ uri: state.api }),
});

const Client = (
  <ApolloProvider client={apolloClient}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </ApolloProvider>
);

rendering(Client, document.getElementById('app'));
