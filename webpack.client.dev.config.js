const path = require('path');

module.exports = {
  mode: 'development',
  devtool: 'inline-source-map',
  entry: './src/app/index.tsx',
  output: {
    path: path.resolve('dist'),
    filename: 'bundle-client.js',
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js'],
    alias: {
      '@': path.resolve('src', 'app'),
    },
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: 'ts-loader',
        options: {
          transpileOnly: true,
        },
      },
      {
        test: /\.scss$/,
        loader: ['css-loader', 'sass-loader'],
      },
    ],
  },
  externals: {
    react: 'React',
    'react-dom': 'ReactDOM',
  },
  devServer: {
    contentBase: ['public', 'dist', 'style'],
    watchContentBase: true,
    writeToDisk: true,
    compress: true,
    open: true,
    watchOptions: {
      ignored: /dist/,
    },
  },
};
