const config = require('./webpack.client.dev.config');

delete config.devtool;
delete config.devServer;
config.mode = 'production';
config.output.filename = 'bundle-client.min.js';
module.exports = config;
