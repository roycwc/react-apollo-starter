Starter Project for Typescript React with Express server-side rendering

Requirements:

- NodeJS version 11.0+
- Linux (or OSX)

# Install Dependencies

```
npm install
```

# How to change name

```
npm run rename [MY_PROJECT_NAME]
```

# How to run in dev mode

It will start the following
Listen on 8080 - client side
Listen on 3100 - server side (no proxy)
Listen on 3000 - server side with auto reload (proxy to 3100)

```
npm run dev
```

# How to build a bundle-client.min.js

JS file include react for client side rendering

```
npm run build
```

# How to run in production mode

```
npm run start
```
